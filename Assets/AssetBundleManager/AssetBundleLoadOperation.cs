﻿using UnityEngine;
using System.Collections;

namespace AssetBundles
{
	public abstract class AssetBundleLoadOperation : IEnumerator
	{
		public object Current
		{
			get
			{
				return null;
			}
		}

		public bool MoveNext()
		{
			return !IsDone();
		}
		
		public void Reset() { }
		
		abstract public bool Update ();
		
		abstract public bool IsDone ();
	}

    
	
	#if UNITY_EDITOR
	public class AssetBundleLoadLevelSimulationOperation : AssetBundleLoadOperation
	{	
		AsyncOperation m_Operation = null;
	
	
		public AssetBundleLoadLevelSimulationOperation (string assetBundleName, string levelName, bool isAdditive)
		{
			string[] levelPaths = UnityEditor.AssetDatabase.GetAssetPathsFromAssetBundleAndAssetName(assetBundleName, levelName);
			if (levelPaths.Length == 0)
			{
				///@TODO: The error needs to differentiate that an asset bundle name doesn't exist
				//        from that there right scene does not exist in the asset bundle...
				
				Debug.LogError("There is no scene with name \"" + levelName + "\" in " + assetBundleName);
				return;
			}
			
			if (isAdditive)
				m_Operation = UnityEditor.EditorApplication.LoadLevelAdditiveAsyncInPlayMode(levelPaths[0]);
			else
				m_Operation = UnityEditor.EditorApplication.LoadLevelAsyncInPlayMode(levelPaths[0]);
		}
		
		public override bool Update ()
		{
			return false;
		}
		
		public override bool IsDone ()
		{		
			return m_Operation == null || m_Operation.isDone;
		}
	}
	
	#endif
	public class AssetBundleLoadLevelOperation : AssetBundleLoadOperation
	{
		protected string 				m_AssetBundleName;
		protected string 				m_LevelName;
		protected bool 					m_IsAdditive;
		protected string 				m_DownloadingError;
		protected AsyncOperation		m_Request;
	
		public AssetBundleLoadLevelOperation (string assetbundleName, string levelName, bool isAdditive)
		{
			m_AssetBundleName = assetbundleName;
			m_LevelName = levelName;
			m_IsAdditive = isAdditive;
		}
	
		public override bool Update ()
		{
			if (m_Request != null)
				return false;
			
			LoadedAssetBundle bundle = AssetBundleManager.GetLoadedAssetBundle (m_AssetBundleName, out m_DownloadingError);
			if (bundle != null)
			{
                m_Request = UnityEngine.SceneManagement.SceneManager.LoadSceneAsync(m_LevelName, m_IsAdditive ? UnityEngine.SceneManagement.LoadSceneMode.Additive : UnityEngine.SceneManagement.LoadSceneMode.Single);
                return false;
			}
			else
				return true;
		}
		
		public override bool IsDone ()
		{
			// Return if meeting downloading error.
			// m_DownloadingError might come from the dependency downloading.
			if (m_Request == null && m_DownloadingError != null)
			{
				Debug.LogError(m_DownloadingError);
				return true;
			}
			
			return m_Request != null && m_Request.isDone;
		}
	}
	
	public abstract class AssetBundleLoadAssetOperation : AssetBundleLoadOperation
	{
		public abstract T GetAsset<T>() where T : UnityEngine.Object;
	}
	
	//public class AssetBundleLoadAssetOperationSimulation : AssetBundleLoadAssetOperation
	//{
	//	Object							m_SimulatedObject;
		
	//	public AssetBundleLoadAssetOperationSimulation (Object simulatedObject)
	//	{
	//		m_SimulatedObject = simulatedObject;
	//	}
		
	//	public override T GetAsset<T>()
	//	{
	//		return m_SimulatedObject as T;
	//	}
		
	//	public override bool Update ()
	//	{
	//		return false;
	//	}
		
	//	public override bool IsDone ()
	//	{
	//		return true;
	//	}
	//}
	
	public class AssetBundleLoadAssetOperationFull : AssetBundleLoadAssetOperation
	{
		protected string 				m_AssetBundleName;
		protected string 				m_AssetName;
		protected string 				m_DownloadingError;
		protected System.Type 			m_Type;
		protected AssetBundleRequest	m_Request = null;
	
		public AssetBundleLoadAssetOperationFull (string bundleName, string assetName, System.Type type)
		{
			m_AssetBundleName = bundleName;
			m_AssetName = assetName;
			m_Type = type;
		}
		
		public override T GetAsset<T>()
		{
			if (m_Request != null && m_Request.isDone)
				return m_Request.asset as T;
			else
				return null;
		}
		
		// Returns true if more Update calls are required.
		public override bool Update ()
		{
			if (m_Request != null)
				return false;
	
			LoadedAssetBundle bundle = AssetBundleManager.GetLoadedAssetBundle (m_AssetBundleName, out m_DownloadingError);
			if (bundle != null)
			{
				///@TODO: When asset bundle download fails this throws an exception...
				m_Request = bundle.m_AssetBundle.LoadAssetAsync (m_AssetName, m_Type);
				return false;
			}
			else
			{
				return true;
			}
		}
		
		public override bool IsDone ()
		{
			// Return if meeting downloading error.
			// m_DownloadingError might come from the dependency downloading.
			if (m_Request == null && m_DownloadingError != null)
			{
				Debug.LogError(m_DownloadingError);
				return true;
			}
	
			return m_Request != null && m_Request.isDone;
		}
	}
	
	public class AssetBundleLoadManifestOperation : AssetBundleLoadAssetOperationFull
	{
		public AssetBundleLoadManifestOperation (string bundleName, string assetName, System.Type type)
			: base(bundleName, assetName, type)
		{
		}
	
		public override bool Update ()
		{
			base.Update();
			
			if (m_Request != null && m_Request.isDone)
			{
				AssetBundleManager.AssetBundleManifestObject = GetAsset<AssetBundleManifest>();
				return false;
			}
			else
				return true;
		}
	}

    /*---------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------
     * 
     * Custom Operation Classes
     * 
     *-------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------*/

    public class AssetBundleLoadOperationAsync : AssetBundleLoadAssetOperation
    {
        protected string m_AssetBundleName;
        protected string m_AssetName;
        //protected System.Type m_Type;
        protected AssetBundleCreateRequest m_Request = null;

        //Properties
        public AssetBundleCreateRequest LoadRequest { set { m_Request = value; } }
        public string AssetBundleName { get { return m_AssetBundleName; } }

        public AssetBundleLoadOperationAsync(string assetBundleName, string assetName)
        {
            m_AssetBundleName = assetBundleName;
            m_AssetName = assetName;
        }

        public override T GetAsset<T>()
        {
            if (m_Request != null && m_Request.isDone)
                return m_Request.assetBundle.LoadAsset<T>(m_AssetName);
            else
                return null;
        }

        public AssetBundle GetLoadedBundle()
        {
            if(m_Request != null && m_Request.isDone)
            {
                return m_Request.assetBundle;
            }
            return null;
        }

        // Returns true if more Update calls are required.
        public override bool Update()
        {
            return !(m_Request != null && m_Request.isDone);
        }

        public override bool IsDone()
        {
            // Return if meeting downloading error.
            // m_DownloadingError might come from the dependency downloading.
            if (m_Request == null)
            {
                Debug.LogError("Load Request of asset " + m_AssetName + " is Null");
                return true;
            }
            
            return m_Request.isDone;
        }
    }

    public class AssetBundleLoadLevelOperationASync : AssetBundleLoadOperationAsync
    {
        protected AsyncOperation m_LevelLoadingOp;
        protected bool m_IsAdditive;

        public AssetBundleLoadLevelOperationASync (string assetBundleName, string levelName, bool isAdditive) : base (assetBundleName, levelName)
        {

        }

        //Return true if the update Function needs more calls
        public override bool Update()
        {
            if(m_Request != null)
            {
                if(m_Request.isDone && m_LevelLoadingOp == null)
                {
                    //We need to load in the level now that the bundle has loaded
                    m_LevelLoadingOp = UnityEngine.SceneManagement.SceneManager.LoadSceneAsync(m_AssetName, m_IsAdditive ? UnityEngine.SceneManagement.LoadSceneMode.Additive : UnityEngine.SceneManagement.LoadSceneMode.Single);
                    return true;
                }
                else if(m_Request.isDone && m_LevelLoadingOp.isDone)
                {
                    //We've loaded the level and are all good
                    return false;
                }
                //Still are either loading asset Bundle or Level in
                return true;
                
            }
            return false;
        }

        public override bool IsDone()
        {
            if(m_Request == null && m_LevelLoadingOp == null)
            {
                return true;
            }

            //If we have loaded the level asset and loaded the level then we are done
            if(m_Request.isDone && m_LevelLoadingOp != null && m_LevelLoadingOp.isDone)
            {
                return true;
            }
            else
            {
                return false;
            }
        }
    }

    public class AssetBundleLoadAssetOperationSimulation : AssetBundleLoadOperationAsync
    {
        Object m_SimulatedObject;

        public AssetBundleLoadAssetOperationSimulation(string assetBundleName, string assetName, Object simulatedObject) : base (assetBundleName, assetName)
        {
            m_SimulatedObject = simulatedObject;
        }

        public override T GetAsset<T>()
        {
            return m_SimulatedObject as T;
        }

        public override bool Update()
        {
            return false;
        }

        public override bool IsDone()
        {
            return true;
        }
    }

    public class AssetBundleLoadLevelOperationSimulationASync : AssetBundleLoadOperationAsync
    {
        AsyncOperation m_Operation = null;


        public AssetBundleLoadLevelOperationSimulationASync(string assetBundleName, string levelName, bool isAdditive) : base (assetBundleName, levelName)
        {
            #if UNITY_EDITOR
            string[] levelPaths = UnityEditor.AssetDatabase.GetAssetPathsFromAssetBundleAndAssetName(assetBundleName, levelName);
            if (levelPaths.Length == 0)
            {
                ///@TODO: The error needs to differentiate that an asset bundle name doesn't exist
                //        from that there right scene does not exist in the asset bundle...

                Debug.LogError("There is no scene with name \"" + levelName + "\" in " + assetBundleName);
                return;
            }

            if (isAdditive)
                m_Operation = UnityEditor.EditorApplication.LoadLevelAdditiveAsyncInPlayMode(levelPaths[0]);
            else
                m_Operation = UnityEditor.EditorApplication.LoadLevelAsyncInPlayMode(levelPaths[0]);
            #endif
        }

        public override bool Update()
        {
            return false;
        }

        public override bool IsDone()
        {
            return m_Operation == null || m_Operation.isDone;
        }
    }
}
