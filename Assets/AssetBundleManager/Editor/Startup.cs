﻿/*--------------------------------------------------------------------------------*
  Copyright (C)Nintendo All rights reserved.

  These coded instructions, statements, and computer programs contain proprietary
  information of Nintendo and/or its licensed developers and are protected by
  national and international copyright laws. They may not be disclosed to third
  parties or copied or duplicated in any form, in whole or in part, without the
  prior written consent of Nintendo.

  The content herein is highly confidential and should be handled accordingly.
 *--------------------------------------------------------------------------------*/

using UnityEditor;
using UnityEditor.SceneManagement;
public static class Startup
{
    public static void Initialize()
    {
        string[] prefabNames = new string[6] {
            "Assets/UnityChan/SD_Kohaku_chanz/Prefabs/Misaki_sum_humanoid.prefab",
            "Assets/UnityChan/SD_Kohaku_chanz/Prefabs/Utc_sum_humanoid.prefab",
            "Assets/UnityChan/SD_Kohaku_chanz/Prefabs/Yuko_sum_humanoid.prefab",
            "Assets/UnityChan/SD_Kohaku_chanz/Prefabs/Misaki_win_humanoid.prefab",
            "Assets/UnityChan/SD_Kohaku_chanz/Prefabs/Utc_win_humanoid.prefab",
            "Assets/UnityChan/SD_Kohaku_chanz/Prefabs/Yuko_win_humanoid.prefab"
        };

        for(int i = 0; i < prefabNames.Length; i++)
        {
            if (prefabNames[i].Contains("sum"))
            {
                AssetImporter.GetAtPath(prefabNames[i]).assetBundleName = "original.assets";
            }
            else
            {
                AssetImporter.GetAtPath(prefabNames[i]).assetBundleName = "aoc.assets";
            }
        }

        AssetBundleScript.BuildOriginalAssetBundles();
        AssetBundleScript.BuildAocAssetBundles();

        if (EditorSceneManager.sceneCount > 0)
        {
            string scenePath = "Assets/Sample/AocAssetBundle.unity";
            EditorSceneManager.OpenScene(scenePath);
        }
        
        EditorUserBuildSettings.SwitchActiveBuildTarget(BuildTargetGroup.Switch, BuildTarget.Switch);
    }
}