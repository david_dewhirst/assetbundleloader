﻿/*--------------------------------------------------------------------------------*
  Copyright (C)Nintendo All rights reserved.

  These coded instructions, statements, and computer programs contain proprietary
  information of Nintendo and/or its licensed developers and are protected by
  national and international copyright laws. They may not be disclosed to third
  parties or copied or duplicated in any form, in whole or in part, without the
  prior written consent of Nintendo.

  The content herein is highly confidential and should be handled accordingly.
 *--------------------------------------------------------------------------------*/

using UnityEditor;
using UnityEngine;
using System.IO;

public static class AssetBundleScript
{
    private static readonly string AssetBundlePathWin64  = "AssetBundle/StandaloneWindows64";
    private static readonly string AssetBundlePathSwitch = "AssetBundle/Switch";
    private static readonly string StreamingAssetsPathWin64 = Application.streamingAssetsPath + "/StandaloneWindows64";
    private static readonly string StreamingAssetsPathSwitch = Application.streamingAssetsPath + "/Switch";
    private static readonly string AocPathSwitch = "SampleAoc/Index1Data";
    private static readonly BuildAssetBundleOptions BuildOption = BuildAssetBundleOptions.ChunkBasedCompression;

    [MenuItem("AocAssetBundle/Build Original AssetBundles")]
    public static void BuildOriginalAssetBundles()
    {
        AssetBundleBuild[] buildMap = new AssetBundleBuild[1];
        buildMap[0].assetBundleName = "original";
        buildMap[0].assetBundleVariant = "";
        buildMap[0].assetNames = new string[3] {
            "Assets/UnityChan/SD_Kohaku_chanz/Prefabs/Misaki_sum_humanoid.prefab",
            "Assets/UnityChan/SD_Kohaku_chanz/Prefabs/Utc_sum_humanoid.prefab",
            "Assets/UnityChan/SD_Kohaku_chanz/Prefabs/Yuko_sum_humanoid.prefab"
        };

        // Win64
        __BuildAndCopyAssetBundles(buildMap, AssetBundlePathWin64, StreamingAssetsPathWin64, BuildTarget.StandaloneWindows64);

        // Switch
        __BuildAndCopyAssetBundles(buildMap, AssetBundlePathSwitch, StreamingAssetsPathSwitch, BuildTarget.Switch);
    }


    [MenuItem("AocAssetBundle/Build AOC AssetBundles")]
    public static void BuildAocAssetBundles()
    {
        AssetBundleBuild[] buildMap = new AssetBundleBuild[1];
        buildMap[0].assetBundleName = "aoc";
        buildMap[0].assetBundleVariant = "";
        buildMap[0].assetNames = new string[3] {
            "Assets/UnityChan/SD_Kohaku_chanz/Prefabs/Misaki_win_humanoid.prefab",
            "Assets/UnityChan/SD_Kohaku_chanz/Prefabs/Utc_win_humanoid.prefab",
            "Assets/UnityChan/SD_Kohaku_chanz/Prefabs/Yuko_win_humanoid.prefab"
        };

        // Win64
        __BuildAndCopyAssetBundles(buildMap, AssetBundlePathWin64, StreamingAssetsPathWin64, BuildTarget.StandaloneWindows64);

        // Switch
        __BuildAndCopyAssetBundles(buildMap, AssetBundlePathSwitch, AocPathSwitch, BuildTarget.Switch);
    }

    [MenuItem("AocAssetBundle/Build DLC AssetBundles")]
    public static void BuildDlcAssetBundles()
    {
        AssetBundleBuild[] buildMap = new AssetBundleBuild[1];
        buildMap[0].assetBundleName = "dlc";
        buildMap[0].assetBundleVariant = "";
        buildMap[0].assetNames = new string[1] {
            "Assets/UnityChan/Prefabs/unitychan_Clean.prefab"
        };

        // Win64
        __BuildAndCopyAssetBundles(buildMap, AssetBundlePathWin64, StreamingAssetsPathWin64, BuildTarget.StandaloneWindows64);

        // Switch
        __BuildAndCopyAssetBundles(buildMap, AssetBundlePathSwitch, StreamingAssetsPathSwitch, BuildTarget.Switch);
    }

    private static void __BuildAndCopyAssetBundles(
    AssetBundleBuild[] buildMap, string outputPath, string copyPath, BuildTarget buildTarget)
    {
        if (!Directory.Exists(outputPath)) { Directory.CreateDirectory(outputPath); }
        BuildPipeline.BuildAssetBundles(outputPath, buildMap, BuildOption, buildTarget);

        for (int i = 0; i < buildMap.Length; i++)
        {
            string src = outputPath + "/" + buildMap[0].assetBundleName;
            string dst = copyPath + "/" + buildMap[0].assetBundleName;
            if (File.Exists(src))
            {
                if (!Directory.Exists(copyPath)) { Directory.CreateDirectory(copyPath); }
                File.Copy(src, dst, true);
            }
        }
    }
}
