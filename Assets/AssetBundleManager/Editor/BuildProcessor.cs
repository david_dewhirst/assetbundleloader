﻿/*--------------------------------------------------------------------------------*
  Copyright (C)Nintendo All rights reserved.

  These coded instructions, statements, and computer programs contain proprietary
  information of Nintendo and/or its licensed developers and are protected by
  national and international copyright laws. They may not be disclosed to third
  parties or copied or duplicated in any form, in whole or in part, without the
  prior written consent of Nintendo.

  The content herein is highly confidential and should be handled accordingly.
 *--------------------------------------------------------------------------------*/

using System.IO;
using UnityEngine;
using UnityEditor.Build;
using UnityEditor;

public class BuildProcessor : IPreprocessBuild, IPostprocessBuild
{
    private static readonly string TMP_DIR = "__tmp";
    private static string assetBundlePath = SetUpBundlePath();

    private static string targetPlatformPath = "";
    private static string tmpPath = "";

    public int callbackOrder { get { return 0; } }

    private static string SetUpBundlePath()
    {
        //Remove Final Character Asset(s)
        string dataPath = Application.dataPath;
        return dataPath.Insert(dataPath.Length - 1, "Bundle") + "/";
    }

    public void OnPreprocessBuild(BuildTarget target, string path)
    {
        string platformName = AssetBundles.Utility.GetPlatformName();
        
        targetPlatformPath = assetBundlePath + platformName;
        tmpPath = Application.streamingAssetsPath + "/" + platformName;

        //The original pathing from 
        //targetPlatformPath = Application.streamingAssetsPath + "/" + platformName;
        //tmpPath = TMP_DIR + "/" + platformName;

        if (Directory.Exists(targetPlatformPath))
        {
            if (Directory.Exists(tmpPath))
            {
                Directory.Delete(tmpPath, true);
            }

            //Directory.CreateDirectory(tmpPath);
            
            Directory.Move(targetPlatformPath, tmpPath);
        }
    }

    public void OnPostprocessBuild(BuildTarget target, string path)
    {
        Directory.Move(tmpPath, targetPlatformPath);
        //Directory.Delete(tmpPath, true);
    }
}