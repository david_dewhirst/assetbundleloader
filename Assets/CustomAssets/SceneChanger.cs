﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class SceneChanger : MonoBehaviour {

    private int currentSceneIndex = 0;
    private int MaxIndex = int.MaxValue;

	// Use this for initialization
	void Start ()
    {
        DontDestroyOnLoad(gameObject);
        
        MaxIndex = UnityEngine.SceneManagement.SceneManager.sceneCountInBuildSettings;
    }

    private void OnGUI()
    {
        GUILayout.Space(Screen.height - 20);
        GUILayout.BeginHorizontal();

        if(currentSceneIndex > 0 && GUILayout.Button("Previous Scene"))
        {
            currentSceneIndex--;
            ChangeScene();
            UnityEngine.SceneManagement.SceneManager.LoadScene(currentSceneIndex, UnityEngine.SceneManagement.LoadSceneMode.Single);
        }

        if(currentSceneIndex < MaxIndex - 1 && GUILayout.Button("Next Scene"))
        {
            currentSceneIndex++;
            ChangeScene();
            UnityEngine.SceneManagement.SceneManager.LoadScene(currentSceneIndex, UnityEngine.SceneManagement.LoadSceneMode.Single);
        }

        GUILayout.Label("Current Scene Index: " + currentSceneIndex + ", Max Scene " + MaxIndex);

        GUILayout.EndHorizontal();
    }

    void ChangeScene()
    {
        GameObject[] DestroyObjects = gameObject.scene.GetRootGameObjects();

        for(int i = 0; i < DestroyObjects.Length; i++)
        {
            if(DestroyObjects[i] != gameObject)
            {
                Destroy(DestroyObjects[i]);
            }
        }
    }
}
