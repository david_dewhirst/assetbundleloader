﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using AssetBundles;
//Similar to AssetBundleManager, only loading from local files. Using Nintendo Method of loading Bundles from Memory than file
public class SwitchBundleManager : MonoBehaviour {

    static string loadingPath = "";
    //Keep track of the Asset Bundles that are currently loaded in, including the dependencies
    private static AssetBundleManifest masterManifest;
    private static Dictionary<string, LoadedBundleInfo> loadedAssetBundles = new Dictionary<string, LoadedBundleInfo>();

    //Variants
    private static string[] m_ActiveVariants;
    public static string[] ActiveVariants { get { return m_ActiveVariants; } set { m_ActiveVariants = value; } }

    /*---------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------
     * 
     * Initalization
     * 
     *-------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------*/

    //Initalize sets up and load the manifest for finding dependencies
    public static void Init()
    {
        Init(Utility.GetPlatformName());
    }

    public static void Init(string PlatformName)
    {
        //Loading Path Shall we set here
        SetUpLoadingPath();

        //Load Master manifest
        LoadSingleBundle(PlatformName, true);
        
        //Return back to program
    }

    private static void SetUpLoadingPath()
    {
        loadingPath = GetStreamingAssetsPath() + "/" + Utility.GetPlatformName();
    }

    private static string GetStreamingAssetsPath()
    {
        return Application.streamingAssetsPath;
        if (Application.isEditor)
        {
            return "file://" + Application.streamingAssetsPath;
        }
        else
        {
            return Application.streamingAssetsPath;
        }
    }

    /*---------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------
     * 
     * Loading Bundles
     * 
     *-------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------*/

    private static void LoadSingleBundle(string assetBundleName, bool isLoadingAssetBundleManifest = false)
    {
        //Check if this bundle has any variants
        string bundleVariation = assetBundleName;
        if(!isLoadingAssetBundleManifest)
            bundleVariation = GetVariationName(assetBundleName);

        string tempLoadingPath = loadingPath + "/" + bundleVariation;
        //Check if the path exists
        if(!System.IO.File.Exists(tempLoadingPath))
        {
            return; //Failed to load something that does not exist
        }

        //Read all data in as array of bytes
        byte[] data = System.IO.File.ReadAllBytes(tempLoadingPath);

        AssetBundle assetBundle = AssetBundle.LoadFromMemory(data);

        if(assetBundle != null)
        {
            loadedAssetBundles.Add(assetBundleName, new LoadedBundleInfo(assetBundle));
        }

        if(isLoadingAssetBundleManifest)
        {
            masterManifest = assetBundle.LoadAsset<AssetBundleManifest>("assetbundlemanifest");
            return;
        }

        //Check if this bundle requires any dependies
    }

    public static AssetBundle LoadAssetBundle(string bundleName)
    {
        List<string> bundleToLoad = new List<string>();

        if(masterManifest == null)
        {
            Debug.LogError("Need to initalize the Bundle manager, to load the Master manifest");
            return null;
        }

        string[] dependencies = masterManifest.GetAllDependencies(bundleName);

        for(int i = 0; i < dependencies.Length; i++)
        {
            if(!loadedAssetBundles.ContainsKey(dependencies[i]))
            {
                bundleToLoad.Add(dependencies[i]);
            }
        }
        
        //Check if the bundle hasn't already been loaded
        if(!loadedAssetBundles.ContainsKey(bundleName))
        {
            bundleToLoad.Add(bundleName);
        }

        for(int i = 0; i < bundleToLoad.Count; i++)
        {
            LoadSingleBundle(bundleToLoad[i]);
        }
        
        if(!loadedAssetBundles.ContainsKey(bundleName))
        {
            //Could not find bundle failed
            Debug.LogError("Could not find bundle: " + bundleName);
            return null;
        }

        return loadedAssetBundles[bundleName].assetBundle;
    }

    public static T LoadAsset<T>(string assetBundleName, string assetname) where T : UnityEngine.Object
    {
        //If the bundle isn't loaded, lets load it in
        if (!loadedAssetBundles.ContainsKey(assetBundleName))
            LoadAssetBundle(assetBundleName);

        LoadedBundleInfo loadedBundle;
        //if the bundle successfull loaded then this shouldn't happen
        if(!loadedAssetBundles.TryGetValue(assetBundleName, out loadedBundle))
        {
            //Could not find bundle failed
            Debug.LogError("Could not find bundle");
            return null;
        }
        
        T asset = loadedBundle.assetBundle.LoadAsset<T>(assetname);

        return asset;
    }

    public static void LoadLevel(string assetBundleName, string levelName, bool isAdditive)
    {
        LoadAssetBundle(assetBundleName);

        if(!loadedAssetBundles.ContainsKey(assetBundleName))
        {
            Debug.LogError("Failed to Load Bundle: " + assetBundleName);
            return;
        }

        UnityEngine.SceneManagement.SceneManager.LoadScene(levelName, isAdditive ? UnityEngine.SceneManagement.LoadSceneMode.Additive : UnityEngine.SceneManagement.LoadSceneMode.Single);
    }

    /*---------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------
     * 
     * Unloading Bundles
     * 
     *-------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------*/

    static public void UnloadAssetBundle(string BundleName)
    {
        UnloadSingleBundle(BundleName);   
        UnloadDependencies(BundleName);
    }

    static protected void UnloadDependencies(string BundleName)
    {

    }

    static protected void UnloadSingleBundle(string BundleName)
    {

    }

    //Comapres the Bundle to the Active Variants, to find the bundle that is closest to that variant
    private static string GetVariationName(string bundleName)
    {
        string[] bundlesWithVariant = masterManifest.GetAllAssetBundlesWithVariant();

        string[] split = bundleName.Split('.');

        int bestFit = int.MaxValue;
        int bestFitIndex = -1;
        // Loop all the assetBundles with variant to find the best fit variant assetBundle.
        for (int i = 0; i < bundlesWithVariant.Length; i++)
        {
            string[] curSplit = bundlesWithVariant[i].Split('.');
            //If they aren't the same bundle then continue to the next bundle name
            if (curSplit[0] != split[0])
                continue;

            //Is the variant name in the array of active Variants?, if not this returns -1
            int found = System.Array.IndexOf(m_ActiveVariants, curSplit[1]);

            // If there is no active variant found. We still want to use the first 
            if (found == -1)
                found = int.MaxValue - 1;

            if (found < bestFit)
            {
                bestFit = found;
                bestFitIndex = i;
            }
        }

        if (bestFit == int.MaxValue - 1)
        {
            Debug.LogWarning("Ambigious asset bundle variant chosen because there was no matching active variant: " + bundlesWithVariant[bestFitIndex]);
        }

        if (bestFitIndex != -1)
        {
            return bundlesWithVariant[bestFitIndex];
        }
        else
        {
            return bundleName;
        }
    }

    public struct LoadedBundleInfo
    {
        public AssetBundle assetBundle;
        public byte RefernceCount;

        public LoadedBundleInfo(AssetBundle newBundle)
        {
            assetBundle = newBundle;
            RefernceCount = 1;
        }
    }
}
