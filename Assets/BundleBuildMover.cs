﻿#if UNITY_EDITOR
using System.IO;
using UnityEditor;
using UnityEditor.Build;
class BundleBuildMover : IPreprocessBuild, IPostprocessBuild
{
    public int callbackOrder { get { return 0; } }
    public void OnPreprocessBuild(BuildTarget target, string path)
    {
        //Remove Any file in streaming Assets

        //Check if there are Asset Bundles for this Platform Target
            
            //If not, build them
        
        //Copy Asset Bundles for our target and paste them into streaming assets
    }

    public void OnPostprocessBuild(BuildTarget target, string path)
    {
        //Remove the the copy we just made of the asset bundles
    }
}
#endif