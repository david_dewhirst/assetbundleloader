﻿using UnityEngine;
using System.Collections;
using AssetBundles;


public class LoadAssets : MonoBehaviour
{
	public const string AssetBundlesOutputPath = "/AssetBundles/";
	public string assetBundleName;
	public string assetName;
    public string[] activeVariants;

	// Use this for initialization
	IEnumerator Start ()
	{
		yield return StartCoroutine(Initialize() );

        AssetBundleManager.ActiveVariants = activeVariants;

		// Load asset.
		yield return StartCoroutine(InstantiateGameObjectAsync (assetBundleName, assetName) );
	}

	// Initialize the downloading url and AssetBundleManifest object.
	protected IEnumerator Initialize()
	{
		// Don't destroy this gameObject as we depend on it to run the loading script.
		DontDestroyOnLoad(gameObject);

        //Set the Path to load the asset bundles from
        AssetBundleManager.SetSourceAssetBundleDirectory(Utility.GetPlatformName());

        // Initialize AssetBundleManifest which loads the AssetBundleManifest object.
        var request = AssetBundleManager.Initialize();
		if (request != null)
			yield return StartCoroutine(request);
	}

	protected IEnumerator InstantiateGameObjectAsync (string assetBundleName, string assetName)
	{
		// This is simply to get the elapsed time for this phase of AssetLoading.
		float startTime = Time.realtimeSinceStartup;

        // Load asset from assetBundle.
        AssetBundleLoadAssetOperation request = AssetBundleManager.LoadAssetAsync(assetBundleName, assetName, typeof(GameObject));
        if (request == null)
            yield break;
        yield return StartCoroutine(request);

        // Get the asset.
        GameObject prefab = request.GetAsset<GameObject>();


        //Load a Asset Instantly (Does pause the game while loading Bundle and Asset Though
        //GameObject prefab = AssetBundleManager.LoadAsset<GameObject>(assetBundleName, assetName);

		if (prefab != null)
			prefab = GameObject.Instantiate(prefab);
		
		// Calculate and display the elapsed time.
		float elapsedTime = Time.realtimeSinceStartup - startTime;
		Debug.Log(assetName + (prefab == null ? " was not" : " was")+ " loaded successfully in " + elapsedTime + " seconds" );
    }

    private void OnDestroy()
    {
        AssetBundleManager.UnloadAssetBundle(assetBundleName);
    }
}
